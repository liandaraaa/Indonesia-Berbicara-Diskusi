package com.indonesiaberbicara.discussionapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.indonesiaberbicara.discussionapp.model.Conclusion;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tv_article_title)
    TextView tvArticeTitle;
    @BindView(R.id.tv_article_date)
    TextView tvArticleDate;
    @BindView(R.id.iv_article_image)
    ImageView ivArticleImage;
    @BindView(R.id.subFloatingCreateArticle)
    FloatingActionButton createArticle;
    @BindView(R.id.subFloatingCreateConclusion)
    FloatingActionButton createConclusion;
    @BindView(R.id.rv_conclusions)
    RecyclerView rvConclusion;

    FirebaseFirestore firebaseFirestore;
    StorageReference storageReference;
    private static final String contentUrl = "content";
    private static final String photoUrl = "photo";
    private static final String title = "title";
    private static final String created = "dd MM yyyy";
    private static final String modified = "dd MM yyyy";

    LinearLayoutManager linearLayoutManager;
    FirestoreRecyclerAdapter rvAdapter;
    DatabaseReference databaseReference;
    EventListener eventListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        firebaseFirestore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        setActiveArticle();
        getConclusionList();
        initLayoutManagerConclusion();


        createArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent article = new Intent(getApplicationContext(), ArticleInputActivity.class);
                startActivity(article);
            }
        });

        createConclusion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent conclusion = new Intent(getApplicationContext(), ConclusionInputActivity.class);
                startActivity(conclusion);
            }
        });

    }

    private void setActiveArticle() {
        DocumentReference activeArticle = firebaseFirestore.collection("article").document("articles");
        activeArticle.get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();

                            StringBuilder titleField = new StringBuilder(" ");
                            titleField.append(documentSnapshot.get(title));
                            tvArticeTitle.setText(titleField);

                            StringBuilder dateField = new StringBuilder(" ");
                            dateField.append(documentSnapshot.get(created));
                            tvArticleDate.setText(dateField);

                            Glide.with(MainActivity.this).load(documentSnapshot.get(photoUrl)).into(ivArticleImage);

                        }
                    }
                });
    }

    private void initLayoutManagerConclusion(){
        linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL, false );
        rvConclusion.setLayoutManager(linearLayoutManager);
    }

    private void getConclusionList(){

        Query query = FirebaseFirestore.getInstance()
                .collection("conclusion").orderBy("created").limit(10);

        FirestoreRecyclerOptions<Conclusion> conclusion = new FirestoreRecyclerOptions.Builder<Conclusion>()
                .setQuery(query, Conclusion.class)
                .build();

        rvAdapter = new FirestoreRecyclerAdapter<Conclusion, ConclusionHolder>(conclusion) {
            @Override
            protected void onBindViewHolder(ConclusionHolder holder, int position, Conclusion model) {
                holder.tvConclusionTitle.setText(model.getTitle());
                holder.tvConclusionDate.setText(model.getCreated());
                holder.tvConclusionContent.setText(model.getContentURL());
                Glide.with(getApplicationContext()).load(model.getPhotoURL()).into(holder.ivConclusionImage);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, "Dilihat", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public ConclusionHolder onCreateViewHolder(ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.list_conclusion, group, false);
                return new ConclusionHolder(view);
            }
        };

        rvAdapter.notifyDataSetChanged();
        rvConclusion.setAdapter(rvAdapter);

    }

    public class ConclusionHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_conclusion_title)
        TextView tvConclusionTitle;
        @BindView(R.id.tv_conlusion_date)
        TextView tvConclusionDate;
        @BindView(R.id.iv_conclusion_image)
        ImageView ivConclusionImage;
        @BindView(R.id.tv_conclusion_content)
        TextView tvConclusionContent;

        public ConclusionHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
