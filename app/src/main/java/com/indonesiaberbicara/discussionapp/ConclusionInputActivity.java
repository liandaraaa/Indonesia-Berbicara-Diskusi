package com.indonesiaberbicara.discussionapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentListenOptions;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.indonesiaberbicara.discussionapp.model.Conclusion;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConclusionInputActivity extends AppCompatActivity {

    @BindView(R.id.iv_conclusion_photos)
    ImageView ivConclusionPhotos;
    @BindView(R.id.et_conclusion_input_title)
    EditText etConclusionInputTitle;
    @BindView(R.id.et_conclusion_input_content)
    EditText etConclusionInputContent;
    @BindView(R.id.b_save_conclusion)
    Button bSaveConclusion;


    FirebaseFirestore firebaseFirestore;
    private static final String contentUrl = "content";
    private static final String photoUrl = "photo";
    private static final String title = "title";
    private static final String created = "dd MM yyyy";
    private static final String modified = "dd MM yyyy";

    FirebaseStorage firebaseStorage;
    StorageReference storageReference;
    @BindView(R.id.iv_photo_conclusion_fromcamera)
    ImageView ivPhotoConclusionFromcamera;
    @BindView(R.id.iv_photo_conclusion_fromgallery)
    ImageView ivPhotoConclusionFromgallery;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Uri selectedImage;
    private String photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conclusion_input);
        ButterKnife.bind(this);

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference().child("conclusionPhotos");

        bSaveConclusion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addConclusion();
            }
        });

        ivPhotoConclusionFromcamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
            }
        });

        ivPhotoConclusionFromgallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
            }
        });
    }

    private String setConclusionDate() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        return dateFormat.format(date);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                selectedImage = data.getData();
                ivConclusionPhotos.setImageBitmap(bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        selectedImage = data.getData();
        ivConclusionPhotos.setImageBitmap(thumbnail);
    }

    private void addConclusion() {


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Sedang memuat. . .");
        progressDialog.show();

        StorageReference conclusionImage = storageReference.child(selectedImage.getLastPathSegment());
        conclusionImage.putFile(selectedImage)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        photo = downloadUrl.toString();

                        Conclusion conclusion = new Conclusion(setConclusionDate(), etConclusionInputTitle.getText().toString(), photo, etConclusionInputContent.getText().toString());

                        firebaseFirestore.collection("conclusion").document("conclusions" + FieldValue.serverTimestamp()).set(conclusion)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        progressDialog.dismiss();

                                        Toast.makeText(ConclusionInputActivity.this, "Kesimpulan berhasil dibuat",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(ConclusionInputActivity.this, "Kesimpulan gagal dibuat",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ConclusionInputActivity.this, "Foto gagal disimpan",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }



}
