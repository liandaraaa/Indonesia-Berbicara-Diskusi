package com.indonesiaberbicara.discussionapp.model;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by zulwiyozaputra on 19/12/17.
 */

public class Article {

    String title;
    String contentURL;
    String photoURL;
    String created;
    List<Message> messages;

    public Article(String created, String title, String photoURL, String contentURL) {
        this.created = created;
        this.title = title;
        this.photoURL = photoURL;
        this.contentURL = contentURL;
    }

    public String getTitle() {
        return title;
    }

    public String getContentURL() {
        return contentURL;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public String getCreated() {
        return created;
    }




    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
