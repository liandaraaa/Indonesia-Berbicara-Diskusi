package com.indonesiaberbicara.discussionapp.model;

import java.util.Date;
import java.util.UUID;

public class Conclusion {

    String created;
    String title;
    String photoURL;
    String contentURL;

    public Conclusion(String created, String title, String photoURL, String contentURL) {
      this.created = created;
      this.title = title;
      this.photoURL = photoURL;
      this.contentURL = contentURL;
    }

    public Conclusion (String  title, String photoURL, String contentURL){

    }

    public String getTitle (){
       return this.title;
    }

    public String getPhotoURL (){
        return this.photoURL;
    }

    public String getContentURL (){
        return this.contentURL;
    }

    public String getCreated(){ return this.created;}
}